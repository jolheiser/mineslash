package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"go.jolheiser.com/mineslash/commands"

	"github.com/peterbourgon/ff/v3"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/disco/slash"
)

func main() {
	fs := flag.NewFlagSet("mineslash", flag.ExitOnError)
	port := fs.Int("port", 8080, "Port to run the handler on")
	clientID := fs.String("client-id", "", "Application ID")
	clientSecret := fs.String("client-secret", "", "Application secret")
	publicKey := fs.String("public-key", "", "Application public key")
	guildID := fs.String("guild-id", "", "Guild ID (if non-global)")
	if err := ff.Parse(fs, os.Args[1:],
		ff.WithEnvVarPrefix("MINESLASH")); err != nil {
		beaver.Error(err)
		return
	}
	httpClient := &http.Client{
		Timeout: time.Second * 5,
	}

	// Create/Update commands
	client := slash.NewClient(*clientID, *clientSecret, slash.WithHTTP(httpClient))
	// Global var bad:tm:, but it works for now
	commands.Client = client

	var appCommands []*slash.ApplicationCommand
	var err error
	if *guildID != "" {
		appCommands, err = client.BulkOverwriteGuildApplicationCommands(context.Background(), *guildID, commands.Commands)
	} else {
		appCommands, err = client.BulkOverwriteGlobalApplicationCommands(context.Background(), commands.Commands)
	}
	if err != nil {
		beaver.Error(err)
		return
	}

	// Bind commands to handlers
	cmds := make([]*slash.Command, len(appCommands))
	for idx, appCommand := range appCommands {
		cmds[idx] = &slash.Command{
			ID:     appCommand.ID,
			Handle: commands.Responses[idx],
		}
	}

	// Create HTTP handler
	handler, err := slash.Handler(*publicKey, cmds)
	if err != nil {
		beaver.Error(err)
		return
	}
	go func() {
		beaver.Infof("http://localhost:%d", *port)
		beaver.Info(client.OAuthURL())
		if err := http.ListenAndServe(fmt.Sprintf(":%d", *port), handler); err != nil {
			beaver.Error(err)
		}
	}()

	// Wait until cancelled
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Kill, os.Interrupt)
	<-ch
}
