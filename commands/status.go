package commands

import (
	"context"
	"fmt"
	"strings"
	"time"

	"go.jolheiser.com/beaver"
	"go.jolheiser.com/disco/embed"
	"go.jolheiser.com/disco/slash"
	"go.jolheiser.com/disco/webhook"
	"go.jolheiser.com/gojang/query"
)

func init() {
	Commands = append(Commands, StatusCommand)
	Responses = append(Responses, StatusResponse)
}

var StatusCommand = &slash.CreateApplicationCommand{
	Name:        "status",
	Description: "Get a server's status via ping or query",
	Options: []*slash.ApplicationCommandOption{
		{
			Name:        "address",
			Description: "Server address",
			Required:    true,
			Type:        slash.StringACOT,
		},
		{
			Name:        "port",
			Description: "Port",
			Type:        slash.IntegerACOT,
		},
	},
}

var StatusResponse = func(interaction *slash.Interaction) (*slash.InteractionResponse, error) {
	address := interaction.Data.Options[0].ValueString()
	port := 25565
	if len(interaction.Data.Options) > 1 {
		port = interaction.Data.Options[1].ValueInt()
	}

	go func() {
		var status statuser
		server := query.NewServer(address, port)

		// First, try query
		timeout := time.Second * 5
		deadline := time.Second * 5
		q, err := server.Query(timeout, deadline)
		if err == nil {
			status = queryStatus{q}
		} else {
			p, err := server.Ping(timeout, deadline)
			if err != nil {
				if err := Client.EditInteractionResponse(context.Background(), interaction.Token, &webhook.WebhookEdit{
					Content: fmt.Sprintf("could not query or ping server at %s:%d", address, port),
				}); err != nil {
					beaver.Error(err)
					return
				}
			}
			status = pingStatus{p}
		}

		e := &embed.Embed{
			Title:       fmt.Sprintf("`%s`", address),
			Description: status.motd(),
			Color:       0x7ed321,
			Fields: []*embed.Field{
				{
					Name:   "Version",
					Value:  status.version(),
					Inline: true,
				},
				{
					Name:   "Players Online",
					Value:  status.ratio(),
					Inline: true,
				},
				{
					Name:  "Players",
					Value: status.players(),
				},
			},
		}
		if err := Client.EditInteractionResponse(context.Background(), interaction.Token, &webhook.WebhookEdit{
			Embeds: []*embed.Embed{
				e,
			},
		}); err != nil {
			beaver.Error(err)
			return
		}
	}()

	return &slash.InteractionResponse{
		Type: slash.DeferredChannelMessageWithSourceIRT,
	}, nil
}

type statuser interface {
	motd() string
	version() string
	ratio() string
	players() string
}

type queryStatus struct {
	*query.Query
}

func (q queryStatus) motd() string {
	return q.CleanMOTD()
}

func (q queryStatus) version() string {
	return fmt.Sprintf("%s %s", q.ServerMod.Name, q.Version)
}

func (q queryStatus) ratio() string {
	return fmt.Sprintf("%d / %d", q.CurrentPlayers, q.MaxPlayers)
}

func (q queryStatus) players() string {
	if len(q.Players) == 0 {
		q.Players = []string{"No players online"}
	}
	return strings.Join(q.Players, ", ")
}

type pingStatus struct {
	*query.Ping
}

func (p pingStatus) motd() string {
	return p.CleanMOTD()
}

func (p pingStatus) version() string {
	return p.Version
}

func (p pingStatus) ratio() string {
	return fmt.Sprintf("%d / %d", p.CurrentPlayers, p.MaxPlayers)
}

func (p pingStatus) players() string {
	return "Ping does not return players"
}
