package commands

import (
	"time"

	"go.jolheiser.com/disco/slash"
	"go.jolheiser.com/gojang"
)

var (
	gojangClient = gojang.New(time.Second * 10)

	Client    *slash.Client
	Commands  = make([]*slash.CreateApplicationCommand, 0)
	Responses = make([]slash.CommandHandleFunc, 0)
)
