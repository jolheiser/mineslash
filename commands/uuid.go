package commands

import (
	"fmt"
	"time"

	"go.jolheiser.com/disco/embed"
	"go.jolheiser.com/disco/slash"
)

func init() {
	Commands = append(Commands, UUIDCommand)
	Responses = append(Responses, UUIDResponse)
}

var UUIDCommand = &slash.CreateApplicationCommand{
	Name:        "uuid",
	Description: "Get a player's UUID (optionally at a specific time)",
	Options: []*slash.ApplicationCommandOption{
		{
			Name:        "player",
			Description: "Minecraft username",
			Required:    true,
			Type:        slash.StringACOT,
		},
		{
			Name:        "at",
			Description: "Timestamp",
			Type:        slash.IntegerACOT,
		},
	},
}

var UUIDResponse = func(interaction *slash.Interaction) (*slash.InteractionResponse, error) {
	username := interaction.Data.Options[0].ValueString()
	at := time.Unix(0, 0)
	if len(interaction.Data.Options) > 1 {
		at = time.Unix(int64(interaction.Data.Options[1].ValueInt()), 0)
	}

	// Get UUID
	profile, err := gojangClient.Profile(username, at)
	if err != nil {
		return nil, err
	}

	e := &embed.Embed{
		Color: 0x7ed321,
		Thumbnail: &embed.Thumbnail{
			URL: fmt.Sprintf("https://minotar.net/helm/%s/100.png", profile.CurrentUsername),
		},
		Fields: []*embed.Field{
			{
				Name:  fmt.Sprintf("%s's UUID", username),
				Value: profile.UUID,
			},
		},
	}

	return &slash.InteractionResponse{
		Type: slash.ChannelMessageWithSourceIRT,
		Data: &slash.InteractionApplicationCommandCallbackData{
			Embeds: []*embed.Embed{
				e,
			},
		},
	}, nil
}
