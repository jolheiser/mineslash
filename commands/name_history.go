package commands

import (
	"fmt"
	"strings"
	"time"

	"go.jolheiser.com/disco/embed"
	"go.jolheiser.com/disco/slash"
)

func init() {
	Commands = append(Commands, NameHistoryCommand)
	Responses = append(Responses, NameHistoryResponse)
}

var NameHistoryCommand = &slash.CreateApplicationCommand{
	Name:        "name-history",
	Description: "Get a player's name history (optionally starting at a specific time)",
	Options: []*slash.ApplicationCommandOption{
		{
			Name:        "player",
			Description: "Minecraft username",
			Required:    true,
			Type:        slash.StringACOT,
		},
		{
			Name:        "at",
			Description: "Timestamp",
			Type:        slash.IntegerACOT,
		},
	},
}

var NameHistoryResponse = func(interaction *slash.Interaction) (*slash.InteractionResponse, error) {
	username := interaction.Data.Options[0].ValueString()
	at := time.Unix(0, 0)
	if len(interaction.Data.Options) > 1 {
		at = time.Unix(int64(interaction.Data.Options[1].ValueInt()), 0)
	}

	// First we need UUID
	resp, err := gojangClient.Profile(username, at)
	if err != nil {
		return nil, err
	}

	// Next is name history
	names, err := gojangClient.UUIDToNameHistory(resp.UUID)
	if err != nil {
		return nil, err
	}

	// Response
	var history string
	for _, name := range names {
		cleaned := strings.NewReplacer("_", "\\_", "*", "\\*").Replace(name.Name)
		history += fmt.Sprintf("\n%s", cleaned)
		if name.ChangedToAt == 0 {
			history += " (original)"
		} else {
			history += fmt.Sprintf(" (%s)", name.ChangedToAtTime().Format("01/02/2006"))
		}
	}

	e := &embed.Embed{
		Color: 0x7ed321,
		Thumbnail: &embed.Thumbnail{
			URL: fmt.Sprintf("https://minotar.net/helm/%s/100.png", names.Current().Name),
		},
		Fields: []*embed.Field{
			{
				Name:  fmt.Sprintf("%s's Name History", username),
				Value: history,
			},
		},
	}

	return &slash.InteractionResponse{
		Type: slash.ChannelMessageWithSourceIRT,
		Data: &slash.InteractionApplicationCommandCallbackData{
			Embeds: []*embed.Embed{
				e,
			},
		},
	}, nil
}
