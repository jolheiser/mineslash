module go.jolheiser.com/mineslash

go 1.16

require (
	github.com/peterbourgon/ff/v3 v3.0.0
	go.jolheiser.com/beaver v1.1.1
	go.jolheiser.com/disco v0.1.4
	go.jolheiser.com/gojang v0.0.6
)
